<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_sets', function (Blueprint $table) {
            $table->id();
            $table->integer('test_set_id')->unique()->nullable();
            $table->string("set_name")->unique();
            $table->string("set_desc");
            $table->string("status")->default('Active');
            $table->boolean("survey_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_sets');
    }
}