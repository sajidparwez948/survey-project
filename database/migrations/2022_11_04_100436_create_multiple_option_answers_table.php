<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMultipleOptionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multiple_option_answers', function (Blueprint $table) {
            $table->id();
            $table->text('statement')->nullable();;
            $table->text('option_statement')->nullable();;
            $table->string('personality_type')->nullable();;
            $table->string('question_id')->nullable();
            $table->string("test_set_id")->nullable();
            $table->text('multiple_type_options')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multiple_option_answer');
    }
}