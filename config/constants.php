<?php

return [

    'fontSmall' => '16px',
    'orderHeaderSize' => '28px',
    'orderSubHeaderSize' => '22px',
    'orderBodySize' => '17px',
    'headerSize' => '22px',
    'subHeaderSize' => '16px',
    'bodySize' => '13.5px',
    'id' => 9677,
    'title' => 'Tentwares',
    'name' => 'Tentwares, Inc',
    'address' => '633 Second St. Manchester, NH 03102',
    'street' => '633 Second St.',
    'city' => 'Manchester',
    'state' => 'NH',
    'zip' => '03102',
    'phone' => '800-245-7116',
    'fax' => '877-820-8129',
    'email' => 'info@tentwares.com',
    'website' => 'www.tentwares.com',
    'VAULT_AUTH_KEY' =>  env('VAULT_AUTH_KEY'),
    'VAULT_USER' => env('VAULT_USER'),
    'VAULT_KEY' => env('VAULT_KEY'),
    'OTP_KEY' => env('OTP_KEY'),
    'perpage' => 20,
    'perpage' => 20,
    'noOfDays' => 365,
];