Nova.booting((Vue, router, store) => {
  router.addRoutes([
    {
      name: 'surveys',
      path: '/surveys',
      component: require('./components/Tool'),
    },
    {
      name: 'questions',
      path: '/questions',
      component: require('./components/Question'),
    },
    {
      name: 'results',
      path: '/results',
      component: require('./components/Results'),
    },
    {
      name: 'create-questions',
      path: '/create-questions',
      component: require('./components/CreateQuestion'),
    },
    {
      name: 'question-detail',
      path: '/question-detail/:questionID?',
      component: require('./components/QuestionDetail'),
    },
    {
      name: 'multiple-type-options',
      path: '/multiple-type-options',
      component: require('./components/MultipleTypeOption'),
    },
    {
      name: 'create-multiple-type-options',
      path: '/create-multiple-type-options',
      component: require('./components/CreateMultipleTypeOption'),
    },
    {
      name: 'multiple-type-option-details',
      path: '/multiple-type-option-detail/:optionID?',
      component: require('./components/MultipleTypeOptionDetail'),
    },


  ])
})
