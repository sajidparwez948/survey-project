<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\SetController;
use App\Http\Controllers\SurveyController;
use App\Http\Controllers\ResultController;
use App\Http\Controllers\OptionController;


/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

// Route::get('/endpoint', function (Request $request) {
//     //
// });


Route::get('/get-questions', [QuestionController::class, 'get_questions']);
Route::get('/get-question/{questionID?}', [QuestionController::class, 'get_questions']);
Route::post('/save-question', [QuestionController::class, 'save_questions']);
Route::post('/delete-question', [QuestionController::class, 'delete_questions']);
Route::put('/update-question', [QuestionController::class, 'update_question']);
Route::get('/get-sets/{survey_id?}', [SetController::class, 'get_sets']);
Route::get('/get-surveys', [SurveyController::class, 'get_surveys']);
Route::get('/get-results', [ResultController::class, 'get_results']);
Route::get('/get-multiple-type-options', [OptionController::class, 'get_multiple_type_option']);
Route::get('/get-multiple-type-options/{id?}', [OptionController::class, 'get_multiple_type_option']);
Route::post('/save-multuple-type-options', [OptionController::class, 'save_multiple_type_options']);

Route::get('/get-answer-type-options', [OptionController::class, 'get_answer_type_options']);