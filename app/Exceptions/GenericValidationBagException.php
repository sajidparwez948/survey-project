<?php

namespace App\Exceptions;

use Illuminate\Support\MessageBag;

class GenericValidationBagException extends \Exception
{
    /** @var MessageBag */
    protected $errors;

    public function __construct(string $message, MessageBag $errors)
    {
        parent::__construct($message, 422);
        $this->errors = $errors;
    }

    public function getErrorBag()
    {
        return $this->errors;
    }

    public function getErrors()
    {
        return $this->errors->toArray();
    }

    public function __toString()
    {
        return sprintf("GenericValidationBagException: %s %s", $this->message, json_encode($this->errors->toArray()));
    }
}