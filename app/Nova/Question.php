<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Hidden;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Select;
use DigitalCreative\ConditionalContainer\ConditionalContainer;
use DigitalCreative\ConditionalContainer\HasConditionalContainer;


class Question extends Resource
{


    use HasConditionalContainer;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Question::class;
    public static $displayInNavigation = false;
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'desc';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */



    const MULTIPLE_OPTION = 'multiple option';
    const RANGE_OPTION = 'range option';



    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            BelongsTo::make('Test Set', 'test_set'),
            // Text::make('Question ID', 'question_id')
            //     ->displayUsing(function ($name) {
            //         return strtoupper($name);
            //     }),
            Text::make('Statement', 'desc'),
            // BelongsTo::make('option', 'test_set'),
            // Text::make('Status', 'status'),
            // Select::make('Question Type')->options([
            //     self::MULTIPLE_OPTION => ['label' => 'Multiple Option'],
            //     self::RANGE_OPTION => ['label' => 'Range Option'],
            // ])->displayUsingLabels(),

            ConditionalContainer::make([
                HasMany::make('Multiple Option Answers', 'multiple_option_answers', 'App\Nova\MultipleOptionAnswer')
            ])->if('question_type ==' . self::MULTIPLE_OPTION),

            ConditionalContainer::make([
                HasMany::make('Range Option Answers'),
            ])->if('question_type ==' . self::RANGE_OPTION)


        ];
    }

    public static function redirectAfterCreate(NovaRequest $request, $resource)
    {
        $que = \App\Models\Question::where('id', $resource->getKey())->first();
        $que->question_id = $resource->getKey();
        $que->save();
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}