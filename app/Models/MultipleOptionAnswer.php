<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MultipleOptionAnswer extends Model
{
    use HasFactory;
    protected $table = 'multiple_option_answers';

    // public function question()
    // {
    //     return $this->belongsTo(Question::class);
    // }
}