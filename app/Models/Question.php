<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    const MULTIPLE_OPTION = 'multiple option';
    const RANGE_OPTION = 'range option';


    public function test_set()
    {
        return $this->belongsTo(TestSet::class, "test_set_id", "test_set_id");
    }

    public function multiple_option_answers()
    {
        return $this->belongsTo(MultipleOptionAnswer::class, 'statement_id', 'id');
    }

    public function range_option_answers()
    {
        return $this->hasMany(RangeOptionAnswer::class, 'question_id');
    }
}