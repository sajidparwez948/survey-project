<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    use HasFactory;



    public function test_sets()
    {
        return $this->hasMany(TestSet::class, "survey_id", "survey_id");
    }
}