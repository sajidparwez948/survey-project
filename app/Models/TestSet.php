<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestSet extends Model
{
    use HasFactory;


    public function questions()
    {
        return $this->hasMany(Question::class, 'test_set_id', "test_set_id");
    }

    public function survey()
    {
        return $this->belongsTo(Survey::class, "survey_id", "survey_id");
    }
}