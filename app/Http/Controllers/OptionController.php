<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MultipleOptionAnswer;
use App\Models\Option;

class OptionController extends Controller
{

    public function get_multiple_type_option(Request $request)
    {


        if ($request->id) {
            return response()->json(MultipleOptionAnswer::where('id', $request->id)->first()->toArray());
        }
        $res = MultipleOptionAnswer::all()->toArray();
        // prx($res);
        return response()->json(['result' => $res]);
    }

    public function get_answer_type_options(Request $request)
    {


        // if ($request->id) {
        //     return response()->json(MultipleOptionAnswer::where('id', $request->id)->first()->toArray());
        // }
        $res = Option::all()->toArray();
        // prx($res);
        return response()->json(['result' => $res]);
    }


    public function save_multiple_type_options(Request $request)
    {

        $option = new MultipleOptionAnswer;
        $option->statement = $request->statement;


        $option->multiple_type_options = json_encode($request->multipleTypeOptions);
        try {
            $option->save();

            return response()->json([$option]);
        } catch (\Exception $th) {
            throw $th;
        }
    }
}