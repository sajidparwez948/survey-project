<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Question;
use App\Models\TestSet;
use App\Models\UserResult;
use App\Models\Option;

class QuestionController extends Controller
{
    public function get_questions(Request $request)
    {

        if ($request->questionID) {
            return response()->json(Question::with(['test_set', 'test_set.survey'])->where('id', $request->questionID)->first()->toArray());
        }

        return response()->json(Question::with(['test_set', 'test_set.survey'])->get()->toArray());
    }
    public function delete_questions(Request $request)
    {

        if ($request->question_id) {
            try {
                Question::where('question_id', $request->question_id)->delete();
                return response()->json(["success" => true, 'message' => "Successfully deleted!"]);
            } catch (\Exception $e) {
                throw $e;
            }
        }

        return response()->json(["success" => false, 'message' => "Resource not deleted! some error occurred"]);
    }


    public function get_user_questions(Request $request)
    {
        $result = [];

        $next_set_id  = null;

        $questionSets = TestSet::with(['questions', 'survey'])
            ->whereHas('survey', function ($q) use ($request) {
                $q->where("survey_id", $request->survey);
            });



        if ($request->set && $request->set == $questionSets->count()) {

            return response()->json(["result" => null, "next_set_id" => null, "test_set_ended" => true]);
        }

        // if (Session::has('tests_set_ids') && $questionSets->count() == count(session('tests_set_ids'))) {
        //     return response()->json(["result" => null, "next_set_name" => null, "test_set_ended" => true]);
        // }

        // if (!empty($request->set)) {
        //     $questionSets = $questionSets->where('test_set_id', $request->set);
        // } else {
        //     $questionSets->inRandomOrder();
        // }

        // $test_set_ids = (clone $questionSets)->pluck('test_set_id');

        if ($request->set) {
            $questionSets->skip($request->set);
            $next_set_id = $request->set + 1;
        } else {
            $next_set_id = 1;
        }

        $questionSets = $questionSets->get() ? $questionSets->get()->toArray() : [];




        if (count($questionSets) > 0) {
            // if (!Session::has('tests_set_ids')) {
            //     Session::put('tests_set_ids', array("hi"));
            // } else {
            //     array_push(session('tests_set_ids'), $questionSet['test_set_id']);
            // }
            // $request->session()->put('test_set_ids', 'value');
            // var_dump(Session::get('test_set_ids'));
            // exit;

            // foreach ($test_set_ids as $id) {
            //     if (!in_array($id, session('test_set_ids'))) {
            //         $next_set_id = $id;
            //         break;
            //     }
            // }

            // while (!in_array($questionSets[$index]['test_set_id'],session('test_set_ids'))) {
            //     $index = rand(0, count($questionSets) - 1);
            //     $questionSets[$index]['test_set_id'];
            // }

            foreach ($questionSets as $questionSet) {


                if (!empty($questionSet["questions"])) {

                    foreach ($questionSet["questions"] as $question) {

                        $quest['id'] = $question['id'];
                        $quest['survey_id'] = $questionSet['survey']['survey_id'];
                        $quest['survey_name'] = $questionSet['survey']['survey_name'];
                        $quest['text'] = $question['desc'];
                        $quest['set'] = $questionSet['test_set_id'];
                        $quest['question_type'] = $question['question_type'];
                        $quest['question_id'] = $question['question_id'];
                        $quest['set_name'] = $questionSet['set_name'];

                        $options = json_decode($question['question_options'], true);
                        $opts = [];
                        foreach ($options as $opt) {
                            $opts[] = $opt['option'];
                        }

                        $quest['options'] = $opts;
                        $result[] = $quest;
                    }

                    break;
                }
            }
        }

        // prx(count($questionSets));
        // session('tests_set_ids');





        return response()->json(["result" => $result, "next_set_id" =>  $next_set_id, "test_set_ended" => false]);
    }

    public function evaluate(Request $request)
    {

        $result = [];
        $responses = $request->responseData;
        $total = $request->totalQues;
        $result = 0;
        $optionsValues = Option::pluck('title')->toArray();

        foreach ($responses as $q_id => $respOptions) {

            $get_q = Question::where('id', $q_id)->first();

            $options = ($get_q) ?  $get_q->question_options : null;

            $options = json_decode($options, true);

            foreach ($options as $option) {
                if ($get_q->question_type == "Range Option Type") {
                    foreach ($respOptions as $opt) {
                        if ($opt['option'] == $option['option']) {
                            $userRes = $opt['val'];
                            $result = ($userRes >= $option['lo'] && $userRes <= $option['hi']) ?  $result + 1 : $result;
                        }
                    }
                } else if ($get_q->question_type == "Multiple Option Type") {

                    foreach ($respOptions as $opt) {
                        // $opt['testType'] =  $optionsValues[0]; //addd temperorily for testing

                        if ($opt['option'] == $option['option']) {
                            $userRes = false;

                            $userRes = ($option['answer_type'][$optionsValues[rand(0, count($optionsValues) - 1)]] ?? 0) ? true : false;

                            $result = ($userRes) ?  $result + 1 : $result;
                        }
                    }
                }
            }

            // if ($questions) {
            //     $questions->
            // }
        }


        $result =  $result * 10;
        $total =  $total * 10;

        $total = $total >= $result ? $total : $result;

        $eligible_for_next_level =  (($result * 100) / $total) >= 40;

        $res = [
            "result" => $result,
            "total" => $total
        ];

        $user = UserResult::where('user_email', $request->email)->first();
        if (!$user) {
            $user = new UserResult;
            $user->user_email = $request->email;
            $user->survey_id =  $request->survey['survey_id'];
        }

        $user->result = json_encode($res);


        $user->save();

        return response()->json(['result' => $res, "is_eligible_for_next_lavel" => $eligible_for_next_level]);
    }


    public function save_questions(Request $request)
    {

        $question = new Question;
        $question->statement_id = $request->statementId;
        $question->test_set_id = $request->test_set_id;
        $question->desc = $request->statement;
        $question->question_type = $request->question_type;
        $question->question_options = json_encode($request->questionOptions);

        try {
            $question->save();
            $question->question_id = $question->id;
            $question->save();
            return response()->json([$question]);
        } catch (\Exception $th) {
            throw $th;
        }
    }


    public function update_question(Request $request)
    {

        $question = Question::with(['test_set'])->where('question_id', $request->question_id)->firstOrFail();
        $question->test_set_id = $request->test_set_id;
        $question->desc = $request->statement;
        $question->question_type = $request->question_type;

        $question->question_options = json_encode($request->questionOptions);
        $question->test_set->survey_id = $request->survey;

        try {
            \DB::beginTransaction();
            $question->test_set->save();
            $question->save();
            \DB::commit();
            return response()->json([$question]);
        } catch (\Exception $th) {
            \DB::rollback();
            throw $th;
        }
    }
}