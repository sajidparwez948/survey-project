<?php

namespace App\Http\Controllers;

use App\Models\UserResult;


use Illuminate\Http\Request;

class ResultController extends Controller
{

    public  function get_results()
    {
        return response()->json(UserResult::with('survey')->get()->toArray());
    }
}