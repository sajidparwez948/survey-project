<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserDetail;
use App\Exceptions\GenericValidationBagException;


class UserController extends Controller
{
    public function registerUser()
    {

        if (request('userId')) {
            $user =  UserDetail::where('email', request('email'))->exists();
            if ($user) {
                throw new \Exception('User already exists');
            }
            try {
                $user = new UserDetail;
                $user->fb_user_id = request('userId');
                $user->fb_user_name = request('name');
                $user->email = request('email');

                $user->save();
                session(['fb_user_id' => request('userId')]);
                session(['fb_user_name' => request('name')]);

                return response()->json([]);
            } catch (\Exception $ex) {
                throw $ex;
            }
        }


        $errors = \Validator::make(request()->all(), [
            'age' => ['required', 'integer'],
            'email' => ['required', 'email'],
            'fname' => ['required'],
            'lname' => ['required']
        ])->errors();

        if ($errors->count()) {
            throw new GenericValidationBagException('There is a problem with your data', $errors);
        }

        $user =  UserDetail::where('email', request('email'))->exists();


        if ($user) {
            throw new \Exception('Email is already registered!');
        } else {
            $user = new UserDetail;
            $user->age = request("age");
            $user->email = request("email");
            $user->fname = request("fname");
            $user->lname = request("lname");
            try {
                $user->save();
                request()->session()->put('email', request("email"));
                return response()->json($user->toArray());
            } catch (\Exception $th) {
                throw $th;
            }
        }
    }
}