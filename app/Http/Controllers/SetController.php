<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TestSet;

class SetController extends Controller
{

    public function get_sets(Request $request)
    {
        return response()->json(TestSet::where('survey_id', $request->survey_id)->get()->toArray());
    }
}