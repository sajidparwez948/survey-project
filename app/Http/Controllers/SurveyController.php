<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Survey;

class SurveyController extends Controller
{
    public function get_surveys(Request $request)
    {
        return response()->json(Survey::all());
    }

    public function getSurvey(Request $request)
    {

        return response()->json(Survey::inRandomOrder()
            ->first());
    }
}